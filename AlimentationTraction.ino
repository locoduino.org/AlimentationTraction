#include <CommandInterpreter.h>
#include "PinDefinitions.h"
#include "Moniteur.h"
#include "RelaiSens.h"
#include "MesureFCEM.h"
#include "CommandePWM.h"
#include "IHM.h"
#include "QuickPort.h"

/* Temporaire, utilise l'IR CAN pour sortir un signal */
QuickPort signal(2);

void setup() {
  relai.init();
  mesureFcem.init();
  commandeMoteur.init();
  signal.setOutput();
  Serial.begin(115200);
  initMoniteur();
}

unsigned long derniereDate = 0;
unsigned long dateInitialeAttenteFCEM = 0;

enum { ATTENTE_MESURE_VITESSE, ATTENTE_FCEM };

byte etat = ATTENTE_MESURE_VITESSE;

void loop() {
  unsigned long date;
  
  switch (etat) {
    case ATTENTE_MESURE_VITESSE:
      date = millis();
      if (date - derniereDate >= 10) {
        /* le moment de la mesure de vitesse est arrivé */
        derniereDate = date;
        dateInitialeAttenteFCEM = micros();
        commandeMoteur.prepareCommande();
        etat = ATTENTE_FCEM;
      }
      break;
    case ATTENTE_FCEM:
      date = micros();
      if (date - dateInitialeAttenteFCEM >= 400) {
        commandeMoteur.commande();
        mesureFcem.razEchantillons();
        etat = ATTENTE_MESURE_VITESSE;
      }
      break;
  }
  leds.maj();
  executeMoniteur();
  signal.writeHIGH();
  signal.writeLOW();
}
