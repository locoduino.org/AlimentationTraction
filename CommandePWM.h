#ifndef __CommandePWM_h__
#define __CommandePWM_h__

#include "Arduino.h"

class PWM 
{
  private:
    byte mValeurPWM;
    
  public:
    PWM() : mValeurPWM(0) {}
    void init() {
      // regle le prescaler du TIMER2 pour monter la fréquence à 32kHz
      TCCR2B &= B11111000;
      TCCR2B |= B00000001;
      continuer();
    }
    void defPWM(byte pwm) { mValeurPWM = pwm; analogWrite(3, pwm); }
    byte pwm()            { return mValeurPWM; }
    void arret()          { defPWM(0); }
    void suspendre()      { analogWrite(3, 0); }
    void continuer()      { analogWrite(3, mValeurPWM); }
};

class CommandePWM {
  private:
    long mGainProportionnel;
    long mGainIntegral;
    long mSommeErreur;
    long mErreurMax;
    int  mErreur;
    int  mConsigne;         /* Valeur désirée */
    byte mDiviseurPWM;      /* Diviseur pour obtenir la PWM */
    bool mAsservi;
    
  public:
    CommandePWM() :
      mGainProportionnel(400),
      mGainIntegral(10),
      mSommeErreur(0),
      mErreurMax(100000),
      mConsigne(0),
      mDiviseurPWM(8),
      mAsservi(false)
    {}
    
    void init();
    void defGainProportionnel(unsigned int gain) { mGainProportionnel = long(gain); }
    void defGainIntegral(byte gain)              { mGainIntegral = long(gain); }
    void defConsigne(byte consigne)              { mConsigne = consigne; }
    void defAsservi(bool asservi)                { mAsservi = asservi; }
    void defDiviseurPWM(byte diviseur)           { mDiviseurPWM = diviseur; }
    void defErreurMax(long erreurMax)            { mErreurMax = erreurMax; }
    void defSommeErreur(long sommeErreur)        { mSommeErreur = sommeErreur; }
    long gainProportionnel()                     { return mGainProportionnel; }
    long gainIntegral()                          { return mGainIntegral; }
    byte consigne()                              { return mConsigne; }
    bool estAsservi()                            { return mAsservi; }
    byte diviseurPWM()                           { return mDiviseurPWM; }
    long erreurMax()                             { return mErreurMax; }
    long sommeErreur()                           { return mSommeErreur; }
    int  erreur()                                { return mErreur; }
    
    void prepareCommande();    
    void commande();    
};

extern CommandePWM commandeMoteur;
extern PWM PWMMoteur;

#endif
