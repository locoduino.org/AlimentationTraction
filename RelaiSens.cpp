#include "RelaiSens.h"
#include "PinDefinitions.h"

RelaiSens::RelaiSens(byte pin)
  : portRelai(pin)
{}

void RelaiSens::init()
{
  portRelai.setOutput();
}

RelaiSens relai(pinRelai);
